#!/usr/bin/python
#-*- coding:utf-8 -*-

################################################################################
#                                   Raspserver                                 #
#   Autor: Àlex Cors                                                           #
#   email: alex.cors.bardolet@gmail.com                                        #
################################################################################
#
#
# Class that deal all the ssh casuistic and how to process all the commands.
#

import logging
import sys
import socket   # Include for socket exceptions
import string

try:
    import paramiko
    logging.getLogger("paramiko").setLevel(logging.WARNING)
    # Avoid paramiko printing all the debug and info stuff    

except:
    logging.error("Paramiko python module not found")
    sys.exit(-1)


from ConfigFileManager import Config as Conf


class Raspserver():

    def __init__(self, user, password):

        self.user = user
        self.password = password
        self.connection = False
        self.connection_type = None

        self.timeout = 3
        self.nbytes = 4096

        self.config_file = Conf()
        self.config_values = self.config_file.getValues()
        self.list_backups = self.config_file.getBackups()


    def getBackups(self):
        return self.list_backups


    def updateBackups(self, list_backups):
        self.list_backups = list_backups
        self.config_file.updateBackups(list_backups)


    def turnoff(self):        
        self.processSSHCommand('sudo halt')
        self.closeSSHConnection()


    def reboot(self):
        self.processSSHCommand('sudo reboot')
        self.closeSSHConnection()


    def startSSHConnection(self):
        """ Start the connection. Start connecting locally and switch to 
            remote if it doesn't work. """
    
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            logging.info("Trying local SSH connection ...")
            self.ssh.connect(self.config_values['local_direction'],
                             int(self.config_values['local_ssh_port']),
                             username=self.user,
                             password=self.password,
                             timeout=self.timeout)
            self.connection = True
            self.connection_type = "Local"
            logging.info("Local SSH connection established correctly.")
        except socket.error:
            logging.warning("Incorrect port or path")
        except paramiko.AuthenticationException:
            logging.warning("Incorrect user or/and password")

#        except:
#            try:
 #               logging.info("Local connection failed. Trying remote SSH connection ...")
#                self.ssh.connect(self.direccio_remota, port=self.port_remot_ssh, username=self.usuari, password=self.contrasenya, timeout=self.timeout)
 #               self.connection = True
 #               self.connection_type = "Remote"
 #               logging.info("Remote SSH connection established correctly.")
 #           except:
 #               self.connection = False
 #               self.connection_type = None
 #               logging.warning("Couldn't connect neither Locally or remotelly.")  

        return self.connection   


    def comprovaConnexioSSh(self):
        """ Comprova si hi ha connexió encara. """

        try:
            exit, stdout, strerr = self.processSSHCommand('echo visca')
            if (exit == True and stdout == 'visca'):
                self.connection = True
            else:
                self.connection = False
        except:
            self.connection = True


    def closeSSHConnection(self):
        
        logging.info("Tancant la connexió SSH ...")
        try:
            self.ssh.close()
            self.connection = False
        except:
            logging.warning("No es pot tancar connexió SSH")


    def processSSHCommand(self, comanda):
        """ Encapsula tota la complicació de processar una ordre per ssh. """

        comanda_exit = False

        if self.connection:
#            try:
            logging.info("Processand comanda %s", comanda)
            stdin, stdout, stderr = self.ssh.exec_command(comanda)

            sortida = stdout.readlines()            
            error = stderr.readlines()
            #error = error[:len(error) - 1]
        
            # strout can be a string or a list of strings
            try:
                logging.debug("stdout => " + sortida)
                sortida = sortida[:len(sortida) - 1]
            except TypeError:
                out_aux = ""
                for line in sortida:
                    out_aux = out_aux + line 
                logging.debug("stdout => " + out_aux)
            #logging.debug("stderr => " + error)

            if len(error) == 0 and (len(sortida) > 0):
                comanda_exit = True
                logging.info("Comanda retornada correctament sense errors.")
            elif len(error) == 0 and len(sortida) == 0:
                comanda_exit = True
                logging.info("Comanda retornada buida però sense errors")
            else:
                comanda_exit = False
                logging.info("Comanda retornada amb errors")
            return comanda_exit, sortida, error

 #           except:
 #               self.connection = False
 #               logging.error("No s'ha pogut enviar la comanda")
 #               return comanda_exit, "", ""
        else:
            logging.warning("No hi ha cap connexió establerta, no es pot enviar comanda")
            return comanda_exit, "", ""


    def addBackup(self, name, origin, origin_location, destination, destination_location):
        self.config_file.addBackup(name, origin, origin_location, destination, destination_location)

#    def checkIfPathExists(self, path):
#        command = 'test -d ' + path + ' && echo exists'
#        command_ok, output, error = self.processSSHCommand(command)

##### Funcions per info estàtica

    def getListOfNonVirtualPartitions(self):

        command = 'lsblk -b'
        command_ok, output, error = self.processSSHCommand(command) 

        list_partitions = list()
       
        # Save the last value of the disc, because in lsblk is always listed
        # first the HDD and then the partition, so the last disk seen is 
        # the owner of the partition.
        last_disk = ""

        for line in output:
            line = line.split()
            if line[5] == "disk":
                last_disk = line[0]
            elif line[5] == "part":
                dict_partition = dict()
                # Filtering weird caracters that means nothing (eye sugar)
                dict_partition["name"] = filter(lambda x: x in string.printable, line[0])
                try:
                    dict_partition["mounting_point"] = self.__validatePathMountingPoint(line[6])
                except IndexError:
                    dict_partition["mounting_point"] = "NO"

                dict_partition["total_space_aprox"] = int(filter(lambda x: x in string.printable, line[3])) / 1024

                dict_partition["hdd"] = last_disk
                list_partitions.append(dict_partition)


        # All the values saved, now apply 'df -h' to each mounting points
        for partition in list_partitions:
            if not partition["mounting_point"] == "NO" and not partition["mounting_point"] == "SWAP":
                command = 'df -k ' + partition["mounting_point"]
                #print command
                command_ok, output, error = self.processSSHCommand(command) 
                output = output[1].split()

                partition["total_space"] = output[1]
                partition["total_used"] = output[2]
                partition["total_free"] = output[3]
                partition["used_percent"] = output[4]
            elif partition["mounting_point"] == "SWAP":
                partition["total_space"] = partition["total_space_aprox"]
                partition["total_used"] = "-"
                partition["total_free"] = "-"
                partition["used_percent"] = "-"
                partition["name"] += " [SWAP]"
            else:
                partition["total_space"] = partition["total_space_aprox"]
                partition["total_used"] = "-"
                partition["total_free"] = "-"
                partition["used_percent"] = "-"
                partition["name"] += " [SYSTEM]"

#        for tuples in list_partitions:
#            print tuples
#        print ""

        return list_partitions


    def checkIfRemotePathExists(self, path):
        """ Check if a remote folder exists by asking information about it. If
            the folder doesn't exists, a error will be returned. """

        comanda = 'df -T ' + path
        comanda_ok, sortida, error = self.processSSHCommand(comanda) 
        
        return comanda_ok


    def getPartitionOfFolder(self, path):
        """ Return the name of the partition where the folder passed as
            parameter is. """

        comanda = 'df -T ' + path
        comanda_ok, sortida, error = self.processSSHCommand(comanda) 
        aux = sortida[1].split(" ")[0] # Isolate the path of the partition
        aux = aux.split("/")[2] # Remove the "/dev/" initial

        return aux


    def getSizeOfFolder(self, path):
        """ Return the size of the folder passed as parameter. """

        comanda = ' du -bs ' + path
        comanda_ok, sortida, error = self.processSSHCommand(comanda) 
        aux = sortida[0].split("\t")[0] # Isolate the path of the partition

        return int(aux)



    def __validatePathMountingPoint(self, path):
        if path[0] == "/":
            return path
        else:
            if path == "[SWAP]":
                return "SWAP"
            else:
                return "NO"


    def __getFrequenciaCPU(self):
        comanda = 'sudo cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq'
        comanda_ok, sortida, error = self.processSSHCommand(comanda) 

        aux1 = sortida[:len(sortida) - 3] + " Mhz"
        return aux1


    def __getRAMTotal(self):
        comanda = 'top -b -n 1 | grep Mem'
        comanda_ok, sortida, error = self.processSSHCommand(comanda)  

        aux1 = sortida.split(":")
        aux2 = aux1[1].split(",")
        aux3 = aux2[0].split(" ")
        memo_kb = int(aux3[4])
        memo_mb = memo_kb / 1024
        return str(memo_mb) + " Mb"


    def __getMidaSD(self):
        comanda = 'sudo fdisk -l | grep "Disk /dev/mmcblk0"'
        comanda_ok, sortida, error = self.processSSHCommand(comanda)  

        aux1 = sortida.split(" ")

        return aux1[2] + " " + aux1[3][:len(aux1[3]) - 1]


    def __getMidaExtern(self):
        comanda = 'sudo fdisk -l | grep "Disk /dev/sda"'
        comanda_ok, sortida, error = self.processSSHCommand(comanda)  

        aux1 = sortida.split(" ")

        return aux1[2] + " " + aux1[3][:len(aux1[3]) - 1]


    def __getSDOcupat(self):
        comanda = 'df -h | grep rootfs'
        comanda_ok, sortida, error = self.processSSHCommand(comanda)  

        aux1 = sortida.split(" ")
        trobat = ""
        for cosa in aux1:
            if cosa.find('%') != -1:
                trobat = cosa
                break
        return trobat


    def __getExternOcupat(self):
        comanda = 'df -h | grep /dev/sda1'
        comanda_ok, sortida, error = self.processSSHCommand(comanda)  

        aux1 = sortida.split(" ")
        trobat = ""
        for cosa in aux1:
            if cosa.find('%') != -1:
                trobat = cosa
                break
        return trobat


    def getInfoEstatica(self):
        """ Passa el diccionari amb la informació estàtica. """
        return self.info_estatica


##### Funcions sobre els processos
        

    def __getStatusTransmission(self):
        comanda = "sudo service transmission-daemon status"
        comanda_ok, sortida, error = self.processSSHCommand(comanda) 
        if "is running" in sortida:
            return "Funcionant"
        else:
            return "Apagat"


    def __getStatusMldonkey(self):
        #return self.getServeiStatus('mldonkey-server')
        return "no va"


    def __getStatusApache(self):
        comanda = "sudo service apache2 status"
        comanda_ok, sortida, error = self.processSSHCommand(comanda) 
        if "is running" in sortida:
            return "Funcionant"
        else:
            return "Apagat"


    def getInfoServeis(self, refresca=False):
        return self.__refrescaInformacioProcessos(refresca)


         

