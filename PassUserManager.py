#!/usr/bin/python
#-*- coding:utf-8 -*-

################################################################################
#                                PassUserManager                               #
#   Autor: Àlex Cors                                                           #
#   email: alex.cors.bardolet@gmail.com                                        #
################################################################################
#
#
# Gestor that saves the user and the password crypted in the '/tmp' folder to
# avoid the user to introduce it in all the oprations.
#


import logging
import base64
from datetime import datetime, timedelta
import os


class Pass():

    def __init__(self):
        self.temp_file = "/tmp/raspruler/lastlogins.conf"

        # Maximum number of minutes when is no need to introduct the user
        # and the password, in MINUTES
        self.max_time_new_login = 5


    def validPassOrUser(self):
        try:
            pass_file = open(self.temp_file)
            logging.debug("Config file opened OK.")
            (in_time, user_read, pass_read) = self.readPassFile(pass_file)
            pass_file.close()
            if in_time:
                return (True, user_read, pass_read)
            else:
                return (False, "", "")
        except IOError as e:
            return (False, "", "")


    def actualStringTime(self):
        return str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))


    def savePassFile(self, user, password):
        try:
            with open(self.temp_file, 'w') as fout:
                fout.write(self.actualStringTime() + "\n")
                fout.write(base64.b64encode(user) + "\n")
                fout.write(base64.b64encode(password) + "\n")
        except IOError:
            os.makedirs(os.path.dirname(self.temp_file))
            with open(self.temp_file, 'w') as fout:
                fout.write(self.actualStringTime() + "\n")
                fout.write(base64.b64encode(user) + "\n")
                fout.write(base64.b64encode(password) + "\n")


    def __checkTimeAgainstActual(self, string_old_data):
        time_now = datetime.now()

        values = string_old_data.split("-")

        time_before = datetime(int(values[0]), int(values[1]), int(values[2]), int(values[3]), int(values[4]), int(values[5]))
        delta_time = time_now - time_before   
     
        logging.debug("Last login => " + str(delta_time))
        max_time_login = time_before + timedelta(minutes=5)

        if (max_time_login > time_now):
            return True
        else:
            return False        


    def readPassFile(self, pass_file):
        i = 0
        for line in pass_file:
            if i == 0:
                in_time = self.__checkTimeAgainstActual(line)
                if in_time:
                    logging.debug("No need for introducing password again")                   
            elif i == 1:
                user = base64.b64decode(line)
            else:
                password = base64.b64decode(line)
            i += 1

        return (in_time, user, password)





