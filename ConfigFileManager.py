#!/usr/bin/python
#-*- coding:utf-8 -*-

################################################################################
#                               ConfigFileManager                              #
#   Autor: Àlex Cors                                                           #
#   email: alex.cors.bardolet@gmail.com                                        #
################################################################################
#
#
# Gestor of the config file; reading, writing and generating if does'nt exist.
#
# Different sorts of parameters are going to be store in this file:
#   VALUE;   which contains connection information with the raspserver
#   BACKUP;  which contains the different backups ready to do
#   FOLDER;  which containts the path to different folders interesting for the
#            user
#

 
import logging


class Config():

    def __init__(self):

        self.name_config_file = "raspserver.conf"
        self.__cleanRegisters()
        
        try:
            config_file = open(self.name_config_file)
        except IOError as e:
            logging.warning("Config file can't be opened. Creating new one ...")
            self.__writeConfigFile()
            config_file = None
        else:
            logging.debug("Config file opened OK.")
            self.__readConfigFile(config_file)


    def __writeConfigFile(self):
        with open(self.name_config_file, 'w') as fout:
            fout.write('##### CONFIG FILE #####\n')
            fout.write('# Do NOT write directly in this file; all the data writed manually will be\n')
            fout.write('# erased by the program itself (It rewrites the config file).\n')
            fout.write('# Edit the parameters running the program.\n')
            fout.write('\n')
            # Write values
            for param, value in self.dict_values.items():
                fout.write('VALUE*' + param + '=' + value + '\n')
            # Write backups
            for backup in self.list_backups:
                fout.write('BACKUP*' + backup["name"] + "=" + backup["path_origin"] + "=" + backup["location_origin"] + "=" + backup["path_destination"] + "=" + backup["location_destination"] + "="+ backup["last_execution"] + '\n')
            # Write folders
            for folder in self.list_folders:
                fout.write('FOLDER*' + folder["name"] + "=" + folder["path"] + '\n')


    def __cleanRegisters(self):
        self.dict_values = {'local_ssh_port':'-',
                            'remote_ssh_port':'-',
                            'local_direction':'-',
                            'remote_direction':'-'}
        self.list_folders = list()
        self.list_backups = list()


    def __readConfigFile(self, config_file):

        self.__cleanRegisters()
        
        for line in config_file:
            if (line[0] != "#" and len(line) > 2):
                types, info = line.split("*", 1)
                if types == "VALUE":
                    param, value = info.split("=", 1)
                    try:
                        self.dict_values[param] = value.rstrip('\n')
                        # removing end of line character
                    except KeyError: 
                        logging.warning("Value in config file not valid. Ignored")
                elif types == "BACKUP":
                    name, origin, location_origin, destination, location_destination, last_update = info.split("=", 6)
                    backup = dict()
                    backup["name"] = name.rstrip('\n')
                    backup["path_origin"] = origin.rstrip('\n')
                    backup["location_origin"] = location_origin.rstrip('\n')
                    backup["path_destination"] = destination.rstrip('\n')
                    backup["location_destination"] = location_destination.rstrip('\n')
                    backup["last_execution"] = last_update.rstrip('\n')
                    self.list_backups.append(backup)
                elif types == "FOLDER":     
                    name, path = info.split("=", 2)
                    folder = dict()
                    folder["name"] = name.rstrip('\n')
                    folder["path"] = path.rstrip('\n')
                    self.list_folders.append(folder)
                else:
                    logging.warning("Value in config file not valid. Ignored")


    def getValues(self):
        return self.dict_values


    def updateValues(self, new_values):
        self.dict_values = new_values
        self.__writeConfigFile()


    def getBackups(self):
        return self.list_backups


    def getFolders(self):
        return self.list_folders


    def addBackup(self, backup):
        self.list_backups.append(backup)
        self.__writeConfigFile()


    def addFolder(self, folder):
        self.list_folders.append(folder)
        self.__writeConfigFile()


    def removeFolderByName(self, name_folder):
        i = 0
        for folder in self.list_folders:
            if folder["name"] == name_folder:
                penis = self.list_folders.pop(i)
                self.__writeConfigFile()
                break
            i += 1

    def addBackup(self, name, path_origin, location_origin, path_destination, location_destination):
        backup = dict()
        backup["name"] = name
        backup["path_origin"] = path_origin
        backup["location_origin"] = location_origin
        backup["path_destination"] = path_destination
        backup["location_destination"] = location_destination
        backup["last_execution"] = "never"
        self.list_backups.append(backup)
        self.__writeConfigFile()
        
            

