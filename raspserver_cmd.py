#!/usr/bin/python
#-*- coding:utf-8 -*-

################################################################################
#                                 Raspserver_cmd                               #
#   Autor: Àlex Cors                                                           #
#   email: alex.cors.bardolet@gmail.com                                        #
################################################################################
#
#
# Interface in the command line for the raspserver class.
#

import getpass
import sys
import logging
import argparse
import os


from ClasseRaspserver import Raspserver as raspserver
from ConfigFileManager import Config as Conf
from PassUserManager import Pass

 
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
subparsers = parser.add_subparsers(title="subcomands", dest = "subcomand")

parser.add_argument("-v", "--verbose", help="Show debug level of information",
        action="store_true")

# STORAGE suboption
parser_storage = subparsers.add_parser ("storage",
        help = "Deal with storage options",
        formatter_class=argparse.RawTextHelpFormatter)
parser_storage.add_argument ("option",        
        help='''Used subcommands:
list            List avaliable storage locations and non virtual partitions, and its usage
list_folders    Like list, but with some extra folders in it, and their size.
add_folder      Add a new folder to be shown
remove_folder   Remove the selected folder''',
        choices=['list', 'add_folder', 'remove_folder', 'list_folders'])

# UPTIME suboption
parser_uptime = subparsers.add_parser("uptime",
        help = "Show how many time the server is on",
        formatter_class=argparse.RawTextHelpFormatter)

# TURNOFF suboption
parser_uptime = subparsers.add_parser("turnoff",
        help = "Turn off the raspserver",
        formatter_class=argparse.RawTextHelpFormatter)

# REBOOT suboption
parser_uptime = subparsers.add_parser("reboot",
        help = "Reboot the raspserver",
        formatter_class=argparse.RawTextHelpFormatter)

# VALUES suboption
parser_values = subparsers.add_parser("values",
        help = "Deal with the connection values",
        formatter_class=argparse.RawTextHelpFormatter)
parser_values.add_argument ("option",        
        help='''Used subcommands:
list    List values stored in the config file
edit    Edit the values stored in the config file''',
        choices=['list', 'edit'])


# BACKUP suboption
parser_backup = subparsers.add_parser("backup",
        help = "Deal with backup options",
        formatter_class=argparse.RawTextHelpFormatter)
parser_backup.add_argument ("option",
        help='''Used subcommands:
list     List available backups
add      Add a new backup
delete   Remove a backup from the list
details  Show details of each bakcup in the list''',
        choices=['list', 'add', 'delete', 'details'])

##### New errors
class ExceptionNotGoodValue( Exception ): pass
class ExceptionExclusiveValues( Exception ): pass
class ExceptionNameAlreadyTaken( Exception ): pass
class ExceptionFolderNotFound( Exception ): pass


##### Edit values

def printValues():
    config_file = Conf()
    dict_values = config_file.getValues()
    print "Values in the config file:"
    for param, value in dict_values.items():
        print(param + ' = ' + value) 


def editValues():
    config_file = Conf()
    dict_values = config_file.getValues()
    for param, value in dict_values.items():
        print "Edit parameter " + param + "? (Now:" + value + ")"
        new_value = raw_input("'N' or enter = No, <new_value> = yes and save:  ")
        print type(new_value)
        if new_value not in ("no", "No", "NO", "n", "N") and new_value != "":
            dict_values[param] = new_value

    config_file.updateValues(dict_values)
    printValues()


##### / Edit values
##### Backup

def addBackup():
    """ There are 3 main concepts to handle; the name of the backup, the folder
        taken as reference in the backup (origin) and the folder where the 
        modifications will be applied (destination). """
    
    rs = getConnection()
    possible_machines = ("local", "server")
    try:
        name_backup = raw_input("Please enter the name of the backup: ")
        backups = rs.getBackups()
        for backup in backups:
            if backup["name"] == name_backup:
                raise ExceptionNameAlreadyTaken()

        origin_path = raw_input("Please enter the origin path: ")
        origin_location = raw_input("Please enter if the folder is local or in the server (local, server): ")
        if not origin_location in possible_machines:
            raise ExceptionNotGoodValue()

        destination_path = raw_input("Please enter the destination path: ")
        destination_location = raw_input("Please enter if the folder is local or in the server (local, server): ")
        if not destination_location in possible_machines:
            raise ExceptionNotGoodValue()

        if destination_location == origin_location:
            raise ExceptionExclusiveValues()

        # Check if the folders exists
        if origin_location == "local" and not checkIfLocalFolderExists(origin_path):
            raise ExceptionFolderNotFound()
        elif destination_path == "local" and not checkIfLocalFolderExists(destination_path):
            raise ExceptionFolderNotFound()

        rs.addBackup(name_backup, origin_path, origin_location, destination_path, destination_location)
    except ExceptionNotGoodValue:
        logging.error("Value can only be local or server. Exiting")
    except ExceptionExclusiveValues:
        logging.error("Detected both folders local or server. They are exclusive values. Exiting")
    except ExceptionNameAlreadyTaken:
        logging.error("Name already taken. Exiting")
    except ExceptionFolderNotFound:
        logging.error("Some of the path are not founded. Exiting")


def printBackups():

    rs = getConnection()
    backups = rs.getBackups()
    if backups:
        for backup in backups:
            print " ----- " + backup["name"] + " ----- "
            print "      Origin:         " + backup["path_origin"] + " (" + backup["location_origin"] + ")"
            print "      Destination:    " + backup["path_destination"] + " (" + backup["location_destination"] + ")"
            print "      Last execution: " + backup["last_execution"]
    else:
        print "There is no backups saved so far"


def deleteBackup():

    rs = getConnection()
    backups = rs.getBackups()
    deletable_backup = raw_input("Please enter the name of the backup to delete: ")
    i = 0
    done = False
    for backup in backups:
        if backup["name"] == deletable_backup:
            backups.pop(i)
            rs.updateBackups(backups)
            done = True
            break
        else:
            i += 1
    if not done:
        logging.warning("Backup not found. Ignoring.")


##### / Backup
##### Cosmetic funcionts to show nice the info from raspserver

def checkIfRemoteFolderExists(path, rs):
    return rs.checkIfRemotePathExists(path)


def checkIfLocalFolderExists(path):
    """ Return True if the local folder exists, or False if it doesn't. """
    return os.path.isdir(path)


def convertKbTo(magnitude, decimals=0):
    """ Convert KiloBytes into a bigger unit. It gives the specified number of
        decimals, with 0 by default. Add the magnitude in the final string. 
        Only accept and dispatch strings.

        If a string is received, it will we sended back. """

    try:
        magnitude = float(magnitude)
        magnitude_converter = { 0 : "Kb", 1 : "Mb", 2 : "Gb", 3 : "Tb", 4 : "Pb"}

        last_decimals = 0
        i = 0
        while (magnitude >= 1024):
            magnitude = magnitude / 1024
            last_decimals = magnitude - int(magnitude)
            i += 1

        if decimals == 0:
            return str(int(magnitude)) + " " + magnitude_converter[i]
        else:
            return str(int(magnitude)) + str(last_decimals)[1:decimals + 2] + " " + magnitude_converter[i]
    except ValueError:
        return magnitude


def fillWithSpaces(init_string, total_to_achieve):
    """ Add as many spaces as needed until the lenght of 'init_string' is
        'total_to_achieve' """

    while len(init_string) < total_to_achieve:
        init_string += ' '
    return init_string


def printListWithFolders():

    raspserver = getConnection()
    list_partitions = raspserver.getListOfNonVirtualPartitions()
    config_file = Conf()
    list_folders = config_file.getFolders()  
    for folder in list_folders:
        folder["partition_owner"] = raspserver.getPartitionOfFolder(folder["path"])

    #      30                            13           13           13           10
    print "PARTITION +[FOLDER]           TOTAL        USED         FREE         % USED"
    
    for partition in list_partitions:
        # Print only the folders with user usefull storage
        if partition["total_used"] != "-": 
            s_name = fillWithSpaces(partition["name"], 30)
            aux = convertKbTo(partition["total_space"], 2)
            s_total = fillWithSpaces(aux, 13)
            aux = convertKbTo(partition["total_used"], 2)
            s_used = fillWithSpaces(aux, 13)
            aux = convertKbTo(partition["total_free"], 2)
            s_free = fillWithSpaces(aux, 13)
            s_percent = fillWithSpaces(partition["used_percent"], 10)

            print s_name + s_total + s_used + s_free + s_percent

            for folder in list_folders:
                if folder["partition_owner"] == partition["name"]:
                    s_name = fillWithSpaces(" " + unichr(9492) + unichr(9472) + "  " + folder["name"], 30)
                    aux = raspserver.getSizeOfFolder(folder["path"]) / 1024
                    s_size_folder = fillWithSpaces(convertKbTo(aux, 2), 15)
                    print s_name + s_size_folder


def printListOfNonVirtualPartitions():

    raspserver = getConnection()
    list_partitions = raspserver.getListOfNonVirtualPartitions()

    #      12          15             13           13           13           14
    print "DISK        PARTITION      TOTAL        USED         FREE         % USED"

    for partition in list_partitions:
        s_disk = fillWithSpaces(partition["hdd"], 12)
        s_part = fillWithSpaces(partition["name"], 15)
        aux = convertKbTo(partition["total_space"], 2)
        s_total = fillWithSpaces(aux, 13)
        aux = convertKbTo(partition["total_used"], 2)
        s_used = fillWithSpaces(aux, 13)
        aux = convertKbTo(partition["total_free"], 2)
        s_free = fillWithSpaces(aux, 13)
        s_percent = fillWithSpaces(partition["used_percent"], 14)

        print s_disk + s_part + s_total + s_used + s_free + s_percent


def addFolderIntoStorage():
    name_folder = raw_input("Please enter the name of the folder: ")
    path_folder = raw_input("Please enter the path of the folder: ")
    rs = getConnection()
    if checkIfRemoteFolderExists(path_folder, rs):   
        new_folder = dict()
        new_folder["name"] = name_folder
        new_folder["path"] = path_folder
        config_file = Conf()
        config_file.addFolder(new_folder)  
    else:
        print 'Remote folder "' + path_folder + '" not found. Adding nothing.'  


def removeFolderFromStorage():
    relacionador = dict()
    config_file = Conf()
    folders = config_file.getFolders() 
    i = 1
    for folder in folders:
        relacionador[str(i)] = folder["name"]

        s_number = fillWithSpaces("(" + str(i) + ")", 6)
        s_name = fillWithSpaces("Name = " + folder["name"],35)
        print s_number + s_name + folder["path"]
        i += 1

    number_option = raw_input("Please enter the number of the folder to delete: ")
    try:
        confirmation = raw_input('Do you surely want to delete the folder "' + relacionador[number_option] + '"? (yes/other) ')
        if confirmation == "yes":
            config_file.removeFolderByName(relacionador[number_option])
        else:
            print "Not deleting the folder."
    except KeyError:
        print "Value not in the list. Avorting."



def getConnection():
    """ Get a raspberry instance for the functions that need to connect
        to the server. """

    # Gather user info
    passwordKeeper = Pass()
    (in_time, user, passwd) = passwordKeeper.validPassOrUser()
    if not in_time:
        user = raw_input("Please enter the name of the user: ")
        passwd = getpass.getpass("Please enter the pass: ")

    rs = raspserver(user, passwd)
    if not rs.startSSHConnection():
        print ("Couldn't connect with the raspserver")
        sys.exit()
    else:
        passwordKeeper.savePassFile(user, passwd)

    return rs



if __name__ == "__main__":

    args = parser.parse_args()
    print args

    # Setting the VERBOSE option
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
        logging.info('Logger initiated in DEBUG level')
    else:
        logging.basicConfig(format='%(levelname)s:%(message)s')

    #Non raspserver depending comands
    if args.subcomand == "values":
        if args.option == "list":
            printValues()
        elif args.option == "edit":
            editValues()
        sys.exit(0)


     
    #Raspserver depending comands
    if args.subcomand == "storage":
        if args.option == "list":
            printListOfNonVirtualPartitions()
        elif args.option == "list_folders":
            printListWithFolders()
        elif args.option == "add_folder":
            addFolderIntoStorage()
        elif args.option == "remove_folder":
            removeFolderFromStorage()
    elif args.subcomand == "uptime":
        pass
    elif args.subcomand == "backup":
        if args.option == "list":
            printBackups()
        elif args.option == "add":
            addBackup()
        elif args.option == "delete":
            deleteBackup()
        elif args.option == "details":
            pass
        else:
            logging.warning("Non expected parameter. Ignoring")
    elif args.subcomand == "turnoff":
        raspserver.turnoff()
    elif args.subcomand == "reboot":
        raspserver.reboot()
    else:
        logging.warning("Non expected subcomand. Ignoring")
    
 

